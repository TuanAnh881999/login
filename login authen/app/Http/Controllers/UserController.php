<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class UserController extends Controller
{
    //
    public function login(Request $request){
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(array('email' => $email, 'password' => $password))) {
            // Authentication passed...
            session(['users' => Auth::user()]);
            return redirect('/');
        }
    }
}
